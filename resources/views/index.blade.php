<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title>Laure Christinat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="ppandp">
    <meta name="Description" content="My Resume "/>
    <link href="css/reset.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/contact.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/progress.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/styles.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/print.css" rel="stylesheet" type="text/css" media="print"/>
    <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="screen">
    <!--[if gt IE 8]><!-->
    <link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <!--<![endif]-->
    <!--[if !IE]>
    <link href="/css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <![endif]-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,600" rel="stylesheet"
          type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Lora:400,400italic" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../public/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../public/favicon.ico" type="image/x-icon">
    <script src="js/modernizr.custom.js" type="text/javascript"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-75035297-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div id="status">
        <div class="parent">
            <div class="child">
                <p class="small">loading</p>
            </div>
        </div>
    </div>
</div>
<!-- end preloader -->
<!--<section class="intro section" id="section1">
  <div class="overlay">
    <div id="headline_cycler">
      <div class="headline_cycler_centralizer">
        <ul class="flexslider">
          <li class="slide first">
          <!-- obi/obi15.jpg Background
            <h2 class="atxt_hl">Laure Christinat</h2>
            <p class="atxt_sl">Architecte d'intérieur</p>
          </li>
          <li class="slide"> </li>
        </ul>
      </div>
    </div>
  </div>
  <a href="#section2" data-title="" id="arrow-down" class="aligncenter">Get started</a> </section>-->
<!-- start header -->

@include('headerBasic')
        <!-- end header -->
<section class="section" id="home">
    <div id="ancor1"></div>
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>À propos</h3>
            <div class="divider">&times;</div>
        </div>
        <div class="col1-1">
            <p class="sub-heading">￼Je suis étudiante à la <a href="#" target="_blank"> Haute Ecole d’Art et de Design
                    en Bachelor d’Architecture d’Intérieur</a> à Genève.
                Actuellement, je suis à la recherche d’un stage d’architecture d’intérieur pendant
                l’été 2016 afin d’acquérir une expérience professionnelle complémentaire à mes études.</p>
        </div>
    </div>


    <div class="container clearfix">

        <div class="big-break"></div>
        <div class="col1-3">
            <h4>Mon profil</h4>
            <img src="images/lchristinat.jpg" id="lilyThumb" />
            <ul class="unordered-list">
                <li> Nom: Laure Christinat</li>
                <li> Date de naissance: 1er Décembre 1992</li>
                <li> Nationalité: Suisse</li>

                <li> <strong class="fa fa-envelope"></strong> <a href="mailto:laure.christinat1052@gmail.com" title="">laure.christinat1052@gmail.com</a>
                </li>
                <li><strong class="fa fa-phone"></strong> +41 (0) 79 229 88 86</li>
            </ul>
        </div>
        <div class="col2-3">
            <h4>Qui suis-je?</h4>
            <p>Actuellement, je suis en deuxième année de Bachelor à la Haute Ecole d’Art et de Design à Genève en
                filière
                architecture d’intérieur. Je souhaite faire un stage de formation afin d’acquérir des compétences plus
                techniques
                dans ce domaine, et d’avoir une première expé- rience professionnelle liée au métier que j’aimerai
                exercer plus tard. J’ai eu l’occasion de faire différents stages dans le domaine commercial lors
                de mon apprentissage qui m’ont beaucoup appris mais comme je me suis réorientée scolairement, j’aimerai
                faire de même au niveau professionnel.</p>
            <p>Je suis habile de mes mains et aime beaucoup le dessin ainsi que trouver des concepts. J’apprécie aussi
                beaucoup le contact avec les personnes et pense être à même de comprendre et traduire ce qu’elles
                désirent.
                Ma première année de Bachelor m’amène dans un nouveau milieu que je ne connaissais pas et que j’apprécie
                beaucoup au niveau des opportunités qui permettent de créer diverses choses très conceptuelles et très
                créatives
                mais j’aimerai vraiment avoir des bases plus terre à terre liées à la technique. Le fait de participer
                à des projets réalisables et réalistes m’enchante, ainsi que de voir les réalisations à très grandes
                échelles.
                Aussi, je pourrai mettre en œuvre les cours théoriques qui me sont enseignés à la HEAD.</p>
            <p>
                Les opportunités de création des réseaux sont inouïes ainsi que la rencontre de personnes de différents
                métiers. De plus, j’aime beaucoup travailler en groupe ; c’est très enrichissant et cela permet
                souvent
                d’avoir des approches différentes sur un même sujet. </p>
        </div>
    </div>

</section>

<section class="section" id="education">
    <div id="ancor3"></div>
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>Formation</h3>
            <div class="divider">&times;</div>
        </div>
        <div class="col1-1">
            <p class="sub-heading">Ma formation du gymnase jusqu’à la haute école.</p>
        </div>
        <div class="col1-1">
            <div class="accordion">
                <div class="tab clearfix closed">
                    <div class="tab-arrow"></div>
                    <div class="time-range"><span> Jan </span> 2014</div>
                    <div class="employer">
                        <h4>HEAD Genève</h4>
                        <div class="hidden-content">
                            <!-- acordion content here -->
                            <div class="tiny-break"></div>
                            <h5>Haute Ecole d’Art et de Design
                                <small>Bachelor d’Architecture d’Intérieur</small>
                            </h5>
                            <p>Architecture d’intérieur, Objets de design,
                                Objets artistiques dans le cadre d’une scénographie d’exposition</p>
                        </div>
                        <!-- / acordion content -->
                    </div>
                </div>

                <!---->
                <div class="tab clearfix closed">
                    <div class="tab-arrow"></div>
                    <div class="time-range"><span> Aoû </span> 2013 - <span> Juil </span>2014</div>
                    <div class="employer">
                        <h4>CFPAA Genève</h4>
                        <div class="hidden-content">

                            <!-- acordion content here -->
                            <div class="tiny-break"></div>
                            <h5>Centre de Formation Professionnel des Arts Appliqués
                                <p>
                                    <small>Année préparatoire voie Architecture d’Intérieur</small>
                                </p>
                            </h5>
                            <p>Architecture d’intérieur, Objets de design, Dessins, Installations, Photographies</p>

                            <!-- / acordion content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


{{--TODO reste des écoles--}}
    </div>
</section>

<section class="section no-border" id="proexperiences">
    <div id="ancor4"></div>
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>Expériences professionelles</h3>
            <div class="divider">&times;</div>
        </div>

        <div class="col1-1">
            <p class="sub-heading">Expériences depuis mon apprentissage à maintenant dans
                les voies commerciale et architecture d’intérieur.</p>
        </div>
        <div class="col1-1">
            <div class="accordion">
                @include('exppro.index')

            </div>
        </div>
    </div>
</section>


<section class="section" id="skills">
    <div id="ancor5"></div>
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>Compétences</h3>
            <div class="divider">&times;</div>
        </div>
        <div class="col1-1">
            <p class="sub-heading">Passionnée par l’art et l’écologie.
                Très bonne capacité à m’intégrer dans une équipe. Bonne acceptation des critiques. Dynamique et
                volontaire.</p>
            <ul class="service_box">
                <li class="service_item">
                    <div class="pie_progress" role="progressbar" data-goal="45">
                        <div class="pie_progress__number">0%</div>
                    </div>
                    <div class="icon_box camera"></div>
                    <p class="service_name">Photographie</p>
                </li>
                <li class="service_item">
                    <div class="pie_progress" role="progressbar" data-goal="58">
                        <div class="pie_progress__number">0%</div>
                    </div>
                    <div class="icon_box drawing"></div>
                    <p class="service_name">Dessin</p>
                </li>
                <li class="service_item">
                    <div class="pie_progress" role="progressbar" data-goal="87">
                        <div class="pie_progress__number">0%</div>
                    </div>
                    <div class="icon_box network"></div>
                    <p class="service_name">Social</p>
                </li>
                <li class="service_item">
                    <div class="pie_progress" role="progressbar" data-goal="100">
                        <div class="pie_progress__number">0%</div>
                    </div>
                    <div class="icon_box illustration"></div>
                    <p class="service_name">Design</p>
                </li>
                <li class="service_item">
                    <div class="pie_progress" role="progressbar" data-goal="68">
                        <div class="pie_progress__number">0%</div>
                    </div>
                    <div class="icon_box computer"></div>
                    <p class="service_name">Informatique</p>
                    <ol class="service_details">
                        <li>Archicad</li>
                        <li> Sketch Up</li>
                        <li> InDesign</li>
                        <li>Photoshop</li>
                        <li> Illustrator</li>
                        <li> First Informatique</li>

                    </ol>
                </li>
                <li class="service_item">
                    <div class="pie_progress" role="progressbar" data-goal="88">
                        <div class="pie_progress__number">0%</div>
                    </div>
                    <div class="icon_box quote"></div>
                    <p class="service_name">Langues</p>
                    <ol class="service_details">
                        <li>Français - Langue maternelle</li>
                        <li>Anglais - B2</li>
                        <li>Espagnol - A2</li>
                        <li>Allemand - B2</li>

                    </ol>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="section" id="contactme">
    <div id="ancor6"></div>
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>Contactez-moi</h3>
            <div class="divider">&times;</div>
        </div>
       {{-- <div class="col1-1">
            <p class="sub-heading extra_spacing">Contactez-moi via le formulaire de contact ou écrivez-moi
                par courrier électronique directement. Je vous reviendrai dès que possible.</p>
        </div>--}}
      {{--  <div class="col2-3 col1-3m">
            <div id="contact">
                <form method="post" action="../../../l-christinat.com/contact.php" name="contactform" id="contactform"
                      autocomplete="off">
                    <fieldset>
                        <label for="name" accesskey="U"><span class="required">Votre prénom et nom</span></label>
                        <input name="name" type="text" id="name" size="30" title="Votre prénom et nom"/>
                        <label for="email" accesskey="E"><span class="required">E-mail</span></label>
                        <input name="email" type="text" id="email" size="30" title="E-mail"/>
                        <label for="comments" accesskey="C"><span
                                    class="required">Message</span></label>
                        <textarea name="comments" cols="40" rows="3" id="comments"
                                  title="Message"></textarea>
                        <input type="submit" class="submit" id="submit" value="Envoyer"/>
                        <span id="message"></span>
                    </fieldset>
                </form>
            </div>
        </div>--}}
   <div >
            <div class="info_panel contact_fp">

                {{--<h4>M'écrire directement</h4>--}}
                <p><strong class="fa fa-envelope"> Email:</strong> <a class="email_link" title="Email"
                             href="mailto:laure.christinat1052@gmail.com">laure.christinat1052@gmail.com</a><br></p>

               <p> <strong class="fa fa-phone"> Téléphone: </strong>+41 (0) 79 229 88 86</p>

              </div>
        </div>
    </div>
</section>
@include('footer')
<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="js/retina.min.js" type="text/javascript"></script>
<script src="js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="js/jquery.nav.js" type="text/javascript"></script>
<script src="js/input.fields.js" type="text/javascript"></script>
<script src="js/jquery.form.js" type="text/javascript"></script>
<script src="js/preloader.js" type="text/javascript"></script>
<script src="js/responsive-nav.js" type="text/javascript"></script>
<script src="js/SmoothScroll.js" type="text/javascript"></script>
<script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="js/jquery.sticky.js" type="text/javascript"></script>
<script src="js/jquery-asPieProgress.min.js" type="text/javascript"></script>
<script src="js/jquery.lettering.js" type="text/javascript"></script>
<script src="js/jquery.textillate.js" type="text/javascript"></script>
<script src="js/general-lettering.js" type="text/javascript"></script>
<script src="js/jquery.sliphover.min.js"></script>
<script src="js/custom.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</body>
</html>
