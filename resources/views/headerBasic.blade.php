
<header class="clearfix">
    <h1 id="logo"> <a href="index.html">Laure<br />
            Christinat</a> </h1>
    <div class="tagline"><span>Architecte d'intérieur</span></div>
    <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
    <nav>
        <ul id="nav">

            <li><a href="#home">Accueil</a>
                <div class="menu-line">&times;</div>
            </li>
            {{-- <li><a href="#about">Qui suis-je?</a>
                 <div class="menu-line">&times;</div> //TODO: BUGG with slider
             </li>--}}
            <li><a href="#education">Formation</a>
                <div class="menu-line">&times;</div>
            </li>
            <li><a href="#proexperiences">Expériences</a>
                <div class="menu-line">&times;</div>
            </li>
            <li><a href="#skills">Compétences</a>
                <div class="menu-line">&times;</div>
            </li>

            <li><a href="#contactme">Contact</a>
                <div class="menu-line">&times;</div>
            </li>
            <li class="external "><a href="{{ URL::to('portfolio') }}" target="_blank"><span class="fa fa-lock"></span> Portfolio</a>
                <div class="menu-line">&times;</div>
            </li>

        </ul>
    </nav>
</header>
