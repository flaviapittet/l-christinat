<header class="clearfix">
    <h1 id="logo"> <a href="{{ URL::to('/') }}#home">Laure<br />
            Christinat</a> </h1>
    <div class="tagline"><span>Architecte d'intérieur</span></div>
    <div id="nav-button"> <span class="nav-bar"></span> <span class="nav-bar"></span> <span class="nav-bar"></span> </div>
    <nav>
        <ul id="nav">

            <li><a href="{{ URL::to('/') }}#home">Accueil</a>
                <div class="menu-line">&times;</div>
            </li>
            {{-- <li><a href="#about">Qui suis-je?</a>
                 <div class="menu-line">&times;</div> //TODO: BUGG with slider
             </li>--}}
            <li><a href="{{ URL::to('/') }}#education">Formation</a>
                <div class="menu-line">&times;</div>
            </li>
            <li><a href="{{ URL::to('/') }}#proexperiences">Expériences</a>
                <div class="menu-line">&times;</div>
            </li>
            <li><a href="{{ URL::to('/') }}#skills">Compétences</a>
                <div class="menu-line">&times;</div>
            </li>

            <li><a href="{{ URL::to('/') }}#contactme">Contact</a>
                <div class="menu-line">&times;</div>
            </li>
            <li class="external "><span class="fa fa-lock"></span> Portfolio
                <div class="menu-line">&times;</div>
            </li>

        </ul>
    </nav>
</header>
