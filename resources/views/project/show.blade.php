<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <title>Laure Christinat - Portfolio - {{$project->title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="ppandp">
    <meta name="Description" content="My Resume "/>
    <link href="../css/reset.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../css/contact.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../css/progress.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../css/styles.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="../css/print.css" rel="stylesheet" type="text/css" media="print"/>
    <link href="../css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../css/portfolio.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../css/animate.css" rel="stylesheet" type="text/css" media="screen">
    <!--[if gt IE 8]><!-->
    <link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <!--<![endif]-->
    <!--[if !IE]>
    <link href="../css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <![endif]-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,600" rel="stylesheet"
          type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Lora:400,400italic" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="shortcut icon" href="../public/favicon.ico" type="image/x-icon">
    <script src="js/modernizr.custom.js" type="text/javascript"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-75035297-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>


<body class="post post1">
<!-- Preloader -->

<!-- end preloader -->
<h1 id="showTitle">{{$project->title}}</h1>

{{--<section class="intro section" id="section1">--}}
<a class="close-post" href="{{ URL::to('portfolio/') }}"></a>

{{-- <div class="overlay">
     <div id="headline_cycler">
         <div class="headline_cycler_centralizer">

             <ul class="flexslider">
                 <li class="slide first">
                     <h2 class="atxt_hl"></h2>
                     <h3>{{$project->class}}</h3>
                     <!--Titre, une date, un slider, une zone de texte,-->
                     <p class="atxt_sl">--</p>
                 </li>
                 <li class="slide">test1 </li>
                 <li class="slide">test2 </li>
             </ul>
         </div>
     </div>
 </div>--}}
{{--</section>--}}
<h3>{{$project->class}}</h3>
{{--<section class="section no-border" id="section2">
    <div class="container clearfix">
        <div class="col1-3 col2-3m">
            <div class="info_panel centered">
                <div class="blog-author-picture">
                   --}}{{-- <div class="images round"><img alt="" src="images/blog/about01.jpg" /></div>--}}{{--
                </div>
                <h3>{{$project->title}}</h3>
                <h4>{{$project->class}}</h4>
                <p>{{$project->dateType}}</p>
                <p><strong>Professeur(s):</strong> {{$project->teacher}}</p>
                <p><strong>Dimensions: </strong>{{$project->dimensions}}</p>
                <p><strong>Materiel: </strong>{{$project->materials}}</p>

                <div class="centralizer"> </div>
            </div>
        </div>
        <div class="col2-3 content hentry">
            <p>{{$project->description}}</p>  </div>
    </div>

</section>--}}

<div class="infoProject">
    <p>{{$project->dateType}} |
        @if($project->teacher)
            <strong>Professeur(s):</strong> {{$project->teacher}} |
        @endif


        @if($project->dimensions)
            <strong>Dimensions: </strong>{{$project->dimensions}} |
        @endif


        <strong>Materiel: </strong>{{$project->materials}}</p>
    <div id="projectDescription"><p>{{$project->description}}</p></div>
</div>

<section id="imgsPortfolio">
    <div id="imgBox">
        @foreach($pictures as $picture)
            <img src="{{$picture->url}}" id="imgPortfolioShow"/>
        @endforeach
    </div>
</section>

<div class="flexslider clearfix">


</div>
{{--<section class="section no-border" id="section3">
    <div class="container clearfix z-index">
        <div class="col1-1 centered">
            <div class="icons tags"></div>
            <h4 class="white">Tags</h4>
        </div>
        <div class="col1-1 centered white-space">
            <div class="tiny-break"></div>
            <a href="#">
                <p class="tag">Slideshow<span>3</span></p>
            </a>
            <a href="#">
                <p class="tag">Photography<span>1</span></p>
            </a>
            <a href="#">
                <p class="tag">Wedding<span>22</span></p>
            </a>
            <div class="break"></div>
        </div>
    </div>
    <div class="overlay2"></div>
</section>--}}
{{--<section class="section" id="section4">
    <div class="container clearfix">
        <div class="col1-3 col2-3m">
            <div class="info_panel centered">
                <h4>Robert Smith</h4>
                <p>August 15<span class="superscript">th</span>, 2014
                    <br />
                    <a href="#">Reply</a></p>
            </div>
        </div>
        <div class="col2-3 content">
            <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
        </div>
        <div class="clear"></div>
        <div class="thin-borderline"></div>
        <div class="col1-3 col2-3m">
            <div class="info_panel centered">
                <h4>John Doe</h4>
                <p>August 1<span class="superscript">st</span>, 2014
                    <br />
                    <a href="#">Reply</a></p>
            </div>
        </div>
        <div class="col2-3 content">
            <p> Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
        </div>
        <div class="clear"></div>
        <div class="thin-borderline"></div>
        <div class="col1-3 col2-3m">
            <div class="info_panel centered">
                <h4>Jane Matts</h4>
                <p>July 16<span class="superscript">th</span>, 2014
                    <br />
                    <a href="#">Reply</a></p>
            </div>
        </div>
        <div class="col2-3 content">
            <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
        </div>
    </div>
</section>--}}
{{--<section class="section" id="section8">
    <div id="ancor8"></div>
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>Leave a Comment</h3>
            <div class="break"></div>
        </div>
        <div class="col2-3 col1-3m">
            <div id="contact">
                <form method="post" action="contact.php" name="contactform" id="contactform" autocomplete="off">
                    <fieldset>
                        <label for="name" accesskey="U"><span class="required">Your Name</span></label>
                        <input name="name" type="text" id="name" size="30" title="Your Name" />
                        <label for="email" accesskey="E"><span class="required">Email</span></label>
                        <input name="email" type="text" id="email" size="30" title="Email" />
                        <label for="comments" accesskey="C"><span class="required">Tell us what you think!</span></label>
                        <textarea name="comments" cols="40" rows="3" id="comments" title="Tell us what you think!"></textarea>
                        <input type="submit" class="submit" id="submit" value="Submit" />
                        <span id="message"></span>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>--}}

@include("footer")

<script src="../js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="../js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="../js/retina.min.js" type="text/javascript"></script>
<script src="../js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<!--<script src="js/jquery.nav.js" type="text/javascript"></script>-->
<script src="../js/input.fields.js" type="text/javascript"></script>
<script src="../js/jquery.form.js" type="text/javascript"></script>
<script src="../js/preloader.js" type="text/javascript"></script>
<script src="../js/responsive-nav.js" type="text/javascript"></script>
<script src="../js/SmoothScroll.js" type="text/javascript"></script>
<script src="../js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="../js/jquery.sticky.js" type="text/javascript"></script>
<script src="../js/jquery-asPieProgress.min.js" type="text/javascript"></script>
<script src="../js/jquery.lettering.js" type="text/javascript"></script>
<script src="../js/jquery.textillate.js" type="text/javascript"></script>
<script src="../js/general-lettering.js" type="text/javascript"></script>
<script src="../js/jquery.sliphover.min.js"></script>
<script src="../js/custom.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
</body>

</html>
