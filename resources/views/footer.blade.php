<footer>
    <div class="container clearfix">
        <p><span class="centered small">© 2016, Laure Christinat. Site created
                by <a href="http://www.flaviapittet.com/?utm_source=lily&utm_medium=siteClient&utm_campaign=LilySite" target="_blank">Flavia Pittet</a>. All Rights Reserved.</span> {{--<span
                    class="alignright small">Connect with me&nbsp;&nbsp;&nbsp; <a href="#" target="_blank"
                                                                                  class="social_icon facebook"></a> <a
                        href="#" target="_blank" class="social_icon dribbble"></a> <a href="#" target="_blank"
                                                                                      class="social_icon xing"></a> <a
                        href="#" target="_blank" class="social_icon googleplus"></a> </span>--}}</p>
    </div>
</footer>