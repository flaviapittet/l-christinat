@foreach($exppros as $exppro)




        <div class="tab clearfix closed">
            <div class="tab-arrow"></div>
            <div class="time-range">{{ $exppro->dateStart}} <span></span>
                @if ($exppro->dateEnd == null)
                @else -  {{ $exppro->dateEnd}}

                @endif
            </div>
            <div class="employer">
                <h4>{{ $exppro->company}}</h4>
                <h5>{{ $exppro->jobTitle}}</h5>
                <div class="hidden-content">
                    {{ $exppro->description}}
                </div>

            </div>
        </div>




@endforeach