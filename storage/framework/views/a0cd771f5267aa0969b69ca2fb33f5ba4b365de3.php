<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title>Laure Christinat - Portfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="ppandp">
    <meta name="Description" content="My Resume "/>
    <link href="css/reset.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/contact.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/progress.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/styles.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/print.css" rel="stylesheet" type="text/css" media="print"/>
    <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/portfolio.css" rel="stylesheet" type="text/css" media="screen">
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="screen">
    <!--[if gt IE 8]><!-->
    <link href="css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <!--<![endif]-->
    <!--[if !IE]>
    <link href="/css/retina-responsive.css" rel="stylesheet" type="text/css" media="screen"/>
    <![endif]-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,600" rel="stylesheet"
          type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800" rel="stylesheet" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Lora:400,400italic" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <script src="js/modernizr.custom.js" type="text/javascript"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-75035297-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<!-- Preloader -->

<?php echo $__env->make('project.headerPortfolio', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



<?php /*<?php foreach($projects as $project): ?>
    <ul>
        <li><?php echo e($project->order); ?></li>
        <li><?php echo e($project->title); ?></li>
        <li><?php echo e($project->class); ?></li>
        <li><?php echo e($project->description); ?></li>
        <img src="<?php echo e($project->url); ?>" class="blogimg" alt="<?php echo e($project->name); ?>"/>

    </ul>
<?php endforeach; ?>*/ ?>

<body class="blog">


<section class="section no-border" id="section2">
    <div class="container clearfix">
        <div class="col1-1 centered">
            <h3>Mon Portfolio</h3>
            <div class="divider">&times;</div>
        </div>

    </div>

    <div class="full_width_gallery desktop_items_3 tablet_items_2 mobile_items_1 clearfix">
        <?php foreach($projects as $project): ?>
            <figure class="gallery_item">
                <a href="<?php echo e(URL::to('portfolio/'.$project->project_id)); ?>" title="<?php echo e($project->title); ?>"
                   class="whole-tile"></a>
                <img alt="" src="<?php echo e($project->urlThumb); ?>" id="mainPicProject"/>
                <figcaption class="gallery_item_text show no-hover">
                    <div class="v_centralizer">

                        <div></div>
                        <div><strong><?php echo e($project->title); ?></strong></div>
                    <div><i><?php echo e($project->dateType); ?></i></div>
                    </div>


                    <div class="read-more"><span>Voir le projet</span></div>
                </figcaption>
            </figure>
        <?php endforeach; ?>
    </div>
    <div class="container clearfix">
        <div class="centralizer"><a href="#section2" class="button active">Haut de page</a>
        </div>
    </div>
</section>

<?php echo $__env->make("footer", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="/js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="/js/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="/js/retina.min.js" type="text/javascript"></script>
<script src="/js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<!--<script src="js/jquery.nav.js" type="text/javascript"></script>-->
<script src="/js/input.fields.js" type="text/javascript"></script>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script src="/js/preloader.js" type="text/javascript"></script>
<script src="/js/responsive-nav.js" type="text/javascript"></script>
<script src="/js/SmoothScroll.js" type="text/javascript"></script>
<script src="/js/jquery.flexslider-min.js" type="text/javascript"></script>
<script src="/js/jquery.sticky.js" type="text/javascript"></script>
<script src="/js/jquery-asPieProgress.min.js" type="text/javascript"></script>
<script src="/js/jquery.lettering.js" type="text/javascript"></script>
<script src="/js/jquery.textillate.js" type="text/javascript"></script>
<script src="/js/general-lettering.js" type="text/javascript"></script>
<script src="/js/jquery.sliphover.min.js"></script>
<script src="/js/custom.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</body>

</html>

