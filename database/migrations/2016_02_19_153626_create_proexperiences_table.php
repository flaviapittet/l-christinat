<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProexperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proexperiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer(('order'));
            $table->string('name');
            $table->string('company');
            $table->string('location');
            $table->string('dateStart');
            $table->string('dateEnd');
            $table->string('jobTitle');
            $table->string('description', 500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proexperiences');
    }
}
