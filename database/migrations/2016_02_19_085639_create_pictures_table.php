<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url');
            $table->boolean('isMainPicture');
            $table->string('dir'); //TODO
            $table->string('urlThumb'); //TODO
            $table->string('urlResized'); //TODO
            //FK
            $table->integer('project_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('id')->
            on('projects')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pictures');
    }
}
