<?php

use Illuminate\Database\Seeder;
use App\Picture;
use App\Project;
use App\ProExperience;
use App\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //1. PROJECTS


        /*Model::unguard();
        $this->call('ProjectTableSeeder');*/

        DB::table('projects')->delete();
        DB::table('pictures')->delete();
        DB::table('proexperiences')->delete();
        DB::table('users')->delete();

        //LILY

        Project::create([
            'order' => 1,
            'title' => 'OBI (Objets Bâtis Instantanés)',
            'class' => 'Architecture - Travail collectif',
            'dateType' => 'Workshop - Octobre 2014',
            'teacher' => 'Youri Kravtchenko, assisté par Lawrence Breitling',
            'dimensions' => '9m2',
            'materials' => 'bois',
            'description' => ' L’Institut des sciences du télescope spatial (STSCL), fondé par la NASA, annonce au monde entier qu’une énorme météorite va s’abattre dans la commune de Juriens dans le Jura Suisse.
                Il est précisément prévu qu’elle tombe le 30 octobre 2014 à 8h20, heure locale, à la rue de la Poterie 1.
                Cette météorite a été nommée Wiliamette pour le besoin des recherches.
                Elle est de la taille d’une cabane.
                Actuellement, les sciendesigners mettent au point un système afin que des artistes puissent l’habiter le temps d’un workshop. Rapellons que cet espace est un atelier de poterie qui accueille depuis une quarantaine d’années des artistes de tous horizons.
                Wiliamette sera explosive, il est impératif que ces éminents chercheurs trouvent une solution pour que la vie en son intérieur soit paisible.',

        ]);


        Picture::create([
            'name' => 'Météorite',
            'url' => 'obi02.jpg',
            'dir' => 'obi',
            "urlThumb" => 'thumb/obi/obi02.jpg',
            "urlResized" => "resized/obi/obi02.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Météorite',
            'url' => 'obi03.jpg',
            'dir' => 'obi',
            "urlResized" => "resized/obi/obi03.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Passerelle menant à la météorite',

            'url' => 'obi05.jpg',
            'dir' => 'obi',

            "urlResized" => "resized/obi/obi04.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Passerelle',
            'url' => 'obi07.jpg',
            'dir' => 'obi',

            "urlResized" => "resized/obi/obi07.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Météorite',
            'url' => 'obi09.jpg',
            'dir' => 'obi',
            "urlResized" => "resized/obi/obi09.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Météorite',
            'url' => 'obi10.jpg',
            'dir' => 'obi',
            "urlResized" => "resized/obi/obi10.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Intérieur',
            'url' => 'obi14.jpg',
            'dir' => 'obi',
            "urlResized" => "resized/obi/obi14.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Intérieur',
            'url' => 'obi15.jpg',
            'dir' => 'obi',
            "urlResized" => "resized/obi/obi15.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Intérieur',
            'url' => 'obi16.jpg',
            'dir' => 'obi',
            "urlThumb" => 'thumb/obi/obi02.jpg',
            "urlResized" => "resized/obi/obi02.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '1')->firstOrFail()->id,

        ]);


       Project::create([
            'order' => 2,
            'title' => 'Musica Cromatica Exposition Universelle Milan 2015',
            'class' => 'Boîte à musique - Travail collectif',
            'dateType' => 'Atelier Chimère - Janvier-Août 2015',
            'teacher' => 'Giona Bierens de Han, supervisé par Jérôme Baratelli et Jan Geipel',
            'dimensions' => '187 x 55 x 56 cm',
            'materials' => 'plexiglass, métal, matériel de robotique et musical',
            'description' => 'Musica Cromatica est une installation inspirée des travaux de Neil Harbisson, qui a pour
             but de traduire des couleurs en sons.
            Dans le contexte de l’Exposition Universelle de Milan, nous avons photographié des plats de 10 restaurants
             sélectionnés par la ville de Genève, puis les avons pixelisés, pour en isoler les couleurs qui composent chaque plats.
            Ces couleurs grâce à un code informatique, développé par un ingénieur, seront associées à des sons en 8 bits.
             Ce passage de couleur à son à lieu dans une caisse en plexiglas, composée de quatre compartiments qui rappelle le gramophone.
            Il est désormais possible pour les visiteurs de se nourrir de musique, par le biais de cette installation.',
            'url' => 'http://chimera-milano.ch',
        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica1.jpg',

            'dir' => 'musicaCromatica',
            "urlThumb" => 'thumb/musicaCromatica/musicaCromatica1.jpg',
            "urlResized" => "resized/musicaCromatica/musicaCromatica1.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica2.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica2.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica3.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica3.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica4.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica4.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica5.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica5.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica6.png',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica6.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Musica Cromatica',
            'url' => 'musicaCromatica7.png',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica7.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Exposition Chimera',
            'url' => 'musicaCromatica8.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica8.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Exposition Chimera',
            'url' => 'musicaCromatica9.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica9.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan et coupes',
            'url' => 'musicaCromatica10.png',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica9.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Elevation',
            'url' => 'musicaCromatica11.png',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica11.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Coupe',
            'url' => 'musicaCromatica12.png',

            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica12.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Maquette-Diagramme',
            'url' => 'musicaCromatica13.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica13.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Maquette-Concept ',
            'url' => 'musicaCromatica14.jpg',
            'dir' => 'musicaCromatica',
            "urlResized" => "resized/musicaCromatica/musicaCromatica14.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '2')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 3,
            'title' => 'Cyclones:0 - Humains:1',
            'class' => 'Architecture - Travail individuel',
            'dateType' => 'Atelier Home Mobile - Septembre 2015 - Janvier 2016',
            'teacher' => 'Carlo Parmigiani, assisté par Alice Dunoyer',
            'dimensions' => '6.23 x 4.51 x 5.11 m',
            'materials' => 'bambou, argile, paille, chaux',
            'description' => 'Un cyclone s’abat en moyenne tous les deux ans et demi en Inde.
            L’espérance de vie d’un indien est de 66 ans, ce qui fait qu’il vivra en moyenne 26 cyclones dans sa vie.
            Message d’alerte (diffusé à la radio et à la télévision):
            ALERTE ROUGE, je répète: ALERTE ROUGE! Un cyclone s’est formé au large de Chennai. Dans moins de 3 heures, il sera sur l’Andhra Pradesh.
            Evacuez la zone ou barricadez vous chez vous. Protégez les fenêtres et restez à l’écoute des informations pour savoir quand sortir.
            Les habitants protègent leur maison pour éviter un maximum de dégâts. Ils prennent les rares objets qu’ils doivent mettre à l’abris avec eux dans l’habitat de survie.
            Ils «hermétisent» l’habitat de survie en protégant les fenêtres avec un système de mur coulissant.
            Ils s’enferment à l’intérieur en se tenant au courant de l’avancée du cyclone afin de savoir quand ils pourront sortir.
            A l’intérieur, tout est pensé pour satisfaire les besoins primaires ainsi que pour occuper le temps.
            Quand la famille a le feu vert pour sortir de l’habitat de survie, ils peuvent reconstruire leur habitat sur le côté gauche de l’habitat de survie.
            L’habitat s’ouvre d’ailleurs sur ce côté pour permettre une vie plus agréable. Cela permettera de protéger leur futur maison des prochains cyclones,
            étant donné que les vents soufflent toujours du même côté puisque le cyclone se forme systématiquement dans la mer indienne.
            Les angles du bâtiment sont aux endroits où les vents du cyclone frappent en premier afin que les vents se séparent pour minimiser l’impact du cyclone face aux dégâts.',
            //'url' => 'http://chimera-milano.ch',
        ]);


        Picture::create([
            'name' => 'Planche de présentation Cyclones: 0 - Humains: 1',

            'url' => 'cyclones0umains1.png',
            "urlThumb" => 'thumb/cyclones0humains1/cyclones0humains1.png',
            'dir' => 'cyclones0Humains1',

            "urlResized" => "resized/cyclones0humains1/cyclones0humains1.png",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '3')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan',
            'url' => 'cyclones0humains12.png',
            'dir' => 'cyclones0Humains1',

            "urlResized" => "resized/cyclones0humains1/cyclones0humains12.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '3')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Coupe',
            'url' => 'cyclones0humains13.png',
            'dir' => 'cyclones0Humains1',

            "urlResized" => "resized/cyclones0humains1/cyclones0humains13.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '3')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Coupe',
            'url' => 'cyclones0humains14.png',
            'dir' => 'cyclones0Humains1',

            "urlResized" => "resized/cyclones0humains1/cyclones0humains14.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '3')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Détail',
            'url' => 'cyclones0humains15.png',
            'dir' => 'cyclones0Humains1',

            "urlResized" => "resized/cyclones0humains1/cyclones0humains15.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '3')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => '3D',
            'url' => 'cyclones0humains16.jpg',
            'dir' => 'cyclones0Humains1',

            "urlResized" => "resized/cyclones0humains1/cyclones0humains16.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '3')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 4,
            'title' => 'Museomix',
            'class' => "Scénographie d'événement - Travail collectif",
            'dateType' => 'Workshop - Octobre 2015',
            'teacher' => 'Martine Anderfuhren',
            'dimensions' => '6.23 x 4.51 x 5.11 m',
            'materials' => 'palettes, bois agloméré, feuilles métaliques, vinyle et toiles récupérées',
            'description' => "Museomix est un événement qui se déroule dans un musée différent chaque année. C'est 3 jours pour remixer le musée avec des visiteurs, des graphistes, des designers, des codeurs, des conservateurs, des médiateurs,...
            Elle a aussi pour buts de favoriser la diffusion de la culture numérique et participative dans les musées et tout type de lieu culturel, d'imaginer de nouvelles formes de médiation, de constituer et d'animer une communauté de personnes intéressées par ces sujets.
            Nous avons créer un mobilier adapté pour cet événement. Etant donné que le musée a une identité visuelle forte, il fallait se démarquer afin qu'on comprenne où sont les lieux utilisés par les museomixers et les pièces représentants l'identité du musée.
            Nous avons opté pour des palettes récupérées ainsi que du matériel qui était utilisé l'année précédente pour cet événement.
                 Museomix a décidé de garder cette scénographie pour les prochaines années.",
            'url' => 'http://chimera-milano.ch',
        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix1.jpg',

            "urlThumb" => 'thumb/museomix/museomix1.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix1.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix2.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix2.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix3.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix13.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix4.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix4.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix5.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix5.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix6.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix6.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix7.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix7.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix8.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix8.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix9.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix9.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix10.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix10.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Museomix',
            'url' => 'Museomix11.jpg',
            'dir' => 'museomix',

            "urlResized" => "resized/museomix/museomix11.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '4')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 5,
            'title' => 'Wait-Think-Share',
            'class' => 'Architecture/Mobilier urbain - Travail collectif',
            'dateType' => 'Workshop - Septembre 2015',
            'teacher' => 'Engi Meacock',

            'materials' => 'lambourdes 4 x 4 cm, bois 3 plis et sangles',
            'description' => "Les quatre tableaux installés de part et d'autre des deux poteaux vise à occuper le temps des voyageurs qui transitent à la gare de Genève, en partageant des faits intéressants, nouveaux, étonnants, des blagues ou encore des conseils.
            Des bandelettes de papier sont sur chaque tableau afin que les voyageurs puissent faire un origami dans le bus en ayant pris en photo l'explication.
            Ce système est intuitif mais nous avons quand même prévu un tableau explicatif."
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare1.jpg',


            'dir' => 'waitThinkShare',
            "urlThumb" => 'thumb/waitThinkShare/waitThinkShare1.jpg',
            "urlResized" => "resized/waitThinkShare/waitThinkShare1.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare2.jpg',


            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare2.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare3.jpg',

            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare3.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare4.jpg',

            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare4.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare5.jpg',

            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare5.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare6.jpg',

            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare6.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare7.jpg',

            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare7.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Wait-Think-Share',
            'url' => 'waitThinkShare8.png',

            'dir' => 'waitThinkShare',
            "urlResized" => "resized/waitThinkShare/waitThinkShare8.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '5')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 6,
            'title' => 'La Chute Libre',
            'class' => 'Entre architecture et mobilier - Travail collectif',
            'dateType' => 'Workshop - Septembre 2014',
            'teacher' => 'Koichi Suzuno',
            'dimensions' => '190 x 164 x 340 cm',
            'materials' => 'lambourdes 4 x 4 cm et bois 3 plis',
            'description' => ' La Chute Libre est un espace dédié aux matériaux qui peuvent être récupéré à d’autres fins d’utilisation.
            Cette matériauthèque est destinée à notre filière qui a tendance à acheter des matériaux en grandes quantités et à ne pas tout utiliser.
            Le but étant que chacun puisse prendre ce qui lui convient tout en laissant quelque chose à son tour.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'La Chute Libre',
            'url' => 'laChuteLibre1.jpg',
            "urlThumb" => 'thumb/laChuteLibre/laChuteLibre1.jpg',
            'dir' => 'laChuteLibre',
            "urlResized" => "resized/laChuteLibre/laChuteLibre1.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '6')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'La Chute Libre',
            'url' => 'laChuteLibre2.jpg',
            'dir' => 'laChuteLibre',
            "urlResized" => "resized/laChuteLibre/laChuteLibre2.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '6')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan - Elevations - 3D',
            'url' => 'laChuteLibre3.jpg',
            'dir' => 'laChuteLibre',
            "urlResized" => "resized/laChuteLibre/laChuteLibre3.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '6')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 7,
            'title' => 'Habitatherapie',
            'class' => 'Architecture - Travail individuel',
            'dateType' => 'Atelier Micro-Habitat - Septembre 2014 - Janvier 2015',
            'teacher' => 'Line Fontana, assisté par Christoph Holz',
            'dimensions' => '50m²',
            'materials' => 'plastique thermoformé blanc',
            'description' => "Cet habitat a été conçu pour Jep Gambardella (personnage du film de la Grande Bellezza), qui se trouve être un riche mondain pessimiste.
            Le but étant que la luminothérapie intégrées aux rondeurs de cet habitat le soigne de son mal-être.
            L’habitat est aux vues de tout le monde puisque cet homme aime paraître.
            La partie frontale de l’habitat est un endroit pour recevoir ses amis et faire la fête puisque c’est ce qui le détend et lui fait retrouver le sourire.
            Cet habitat s'inscrit dans un bâtiment des années 1980, anciennement bureaux/dépôt. Ce bâtiment est donc logé d'une population mixte (étudiants, personnes âgées) dans un micro-habitat, parfois mobile.
            Parmi les 30 logements s'imbrique un réseau d'activités à chaque étage (boulangerie, galerie, salon de coiffure, etc).",
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Habitatherapie',
            'url' => 'habitatherapie1.jpg',
            'urlThumb' =>'thumb/habitatherapie/habitatherapie1.jpg',
            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie1.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Intérieur',
            'url' => 'habitatherapie2.jpg',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie2.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Intérieur',
            'url' => 'habitatherapie3.jpg',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie3.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan',
            'url' => 'habitatherapie4.png',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie4.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Coupes',
            'url' => 'habitatherapie5.png',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie5.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Schemas',
            'url' => 'habitatherapie6.png',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie6.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Contexte',
            'url' => 'habitatherapie7.jpg',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie7.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Contexte',
            'url' => 'habitatherapie8.jpg',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie8.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Contexte',
            'url' => 'habitatherapie9.jpg',

            'dir' => 'habitatherapie',

            "urlResized" => "resized/habitatherapie/habitatherapie9.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '7')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 8,
            'title' => 'Spider Stool',
            'class' => 'Mobilier - Travail collectif',
            'dateType' => 'Cours de structure - Décembre 2014 - Janvier 2015',
            'teacher' => 'Irma Cilacian',
            'dimensions' => '40 x 20 x 40 cm',
            'materials' => 'bois 3 plis',
            'description' => 'Ce tabouret est démontable et remontable afin de pouvoir le transporter facilement pour le mettre là où il nous est utile.
            Nous nous sommes basé sur l’araignée, car elle est très stable avec ses pattes malgré la finesse de celles-ci.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Spider Stool',
            'url' => 'spiderStool1.jpg',


            'dir' => 'spiderStool',
            "urlResized" => 'resized/spiderStool/spiderStool1.jpg',

            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Spider Stool',
            'url' => 'spiderStool2.jpg',
            'dir' => 'spiderStool',
            "urlResized" => 'resized/spiderStool/spiderStool2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Spider Stool',
            'url' => 'spiderStool3.jpg',
            'dir' => 'spiderStool',
            "urlThumb" => 'thumb/spiderStool/spiderStool3.jpg',
            "urlResized" => 'resized/spiderStool/spiderStool3.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Spider Stool',
            'url' => 'spiderStool4.jpg',
            'dir' => 'spiderStool',
            "urlResized" => 'resized/spiderStool/spiderStool4.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Planche de présentation',
            'url' => 'spiderStool5.png',
            'dir' => 'spiderStool',
            "urlResized" => 'resized/spiderStool/spiderStool5.png',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Idée',
            'url' => 'spiderStool6.png',
            'dir' => 'spiderStool',
            "urlResized" => 'resized/spiderStool/spiderStool6.png',

            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Concept',
            'url' => 'spiderStool7.png',
            'dir' => 'spiderStool',
            "urlResized" => 'resized/spiderStool/spiderStool7.png',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '8')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 9,
            'title' => 'Tiroirs en typo',
            'class' => 'Mobilier - Travail individuel',
            'dateType' => 'Cours de typographie - Janvier 2014',
            'teacher' => 'Christine Keim',
            'dimensions' => '190 x 70 x 60 cm',
            'materials' => 'carton et papier népalais',
            'description' => ' Ce meuble est un pèse-personne. Il résiste à une huitantaine de kilos.
            Le but était de prouver que le carton malgré son apparence légère et fragile peut être solide tout comme la chaise en carton de Frank O. Gehry, Wiggle Side Chair.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Tiroirs en typo',
            'url' => 'tiroirsEnTypo1.jpg',
            'dir' => 'tiroirsEnTypo',
            "urlResized" => 'resized/tiroirsEnTypo/tiroirsEnTypo1.jpg',
            "urlThumb" => 'thumb/tiroirsEnTypo/tiroirsEnTypo1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '9')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Tiroirs en typo',
            'url' => 'tiroirsEnTypo2.jpg',
            'dir' => 'tiroirsEnTypo',
            "urlResized" => 'resized/tiroirsEnTypo/tiroirsEnTypo2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '9')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Tiroirs en typo',
            'url' => 'tiroirsEnTypo3.jpg',
            'dir' => 'tiroirsEnTypo',
            "urlResized" => 'resized/tiroirsEnTypo/tiroirsEnTypo3.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '9')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Tiroirs en typo',
            'url' => 'tiroirsEnTypo4.jpg',
            'dir' => 'tiroirsEnTypo',
            "urlResized" => 'resized/tiroirsEnTypo/tiroirsEnTypo4.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '9')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 10,
            'title' => 'Lampe',
            'class' => 'Mobilier - Travail personnel',
            'dateType' => 'Avril 2014',

            'materials' => 'carton et papier népalais',
            'description' => 'Cette lampe a été réalisée uniquement avec les déchets de carton des autres meubles en carton.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Lampe',
            'url' => 'lampeCarton1.jpg',

            'dir' => 'lampeCarton',
            "urlResized" => 'resized/lampeCarton/lampeCarton1.jpg',
            "urlThumb" => 'thumb/lampeCarton/lampeCarton1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '10')->firstOrFail()->id,


        ]);


        Picture::create([
            'name' => 'Lampe',
            'url' => 'lampeCarton2.jpg',


            'dir' => 'lampeCarton',
            "urlResized" => 'resized/lampeCarton/lampeCarton2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '10')->firstOrFail()->id,


        ]);


        Picture::create([
            'name' => 'Lampe',
            'url' => 'lampeCarton3.jpg',

            'dir' => 'lampeCarton',
            "urlResized" => 'resized/lampeCarton/lampeCarton3.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '10')->firstOrFail()->id,


        ]);


        Project::create([
            'order' => 11,
            'title' => 'Meuble',
            'class' => 'Mobilier - Travail personnel',
            'dateType' => 'Janvier 2013',
            'dimensions' => '170 x 100 x 40 cm',
            'materials' => 'carton et papier népalais',
            'description' => 'Ce meuble devait avoir deux fonctions: être un meuble à télévision et en même temps un meuble avec des tiroirs pour poser une imprimante dessus et contenir du matériel de bureau.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Meuble',
            'url' => 'meubleCarton1.jpg',
            'dir' => 'meubleCarton',
            "urlResized" => 'resized/meubleCarton/meubleCarton1.jpg',
            "urlThumb" => 'thumb/meubleCarton/meubleCarton1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '11')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Meuble',
            'url' => 'meubleCarton2.jpg',
            'dir' => 'meubleCarton',
            "urlResized" => 'resized/meubleCarton/meubleCarton2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '11')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 12,
            'title' => 'Pouf',
            'class' => 'Mobilier - Travail personnel',
            'dateType' => 'Mars 2014',
            'dimensions' =>"env. 40 x 40 x 25 cm",
            'materials' => 'carton et papier népalais',
            'description' => 'Ce petit pouf résiste à un poids d’environ 60 kilos.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Pouf',
            'url' => 'poufCarton1.jpg',
            'dir' => 'poufCarton',
            "urlResized" => 'resized/poufCarton/poufCarton1.jpg',
            "urlThumb" => 'thumb/poufCarton/poufCarton1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '12')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Pouf',
            'url' => 'poufCarton2.jpg',

            'dir' => 'poufCarton',
            "urlResized" => 'resized/poufCarton/poufCarton2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '12')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 13,
            'title' => 'Bâtiment',
            'class' => 'Architecture - Travail personnel',
            'dateType' => 'Mars 2013',

            'materials' => 'carton et vitre',
            'description' => 'Ce projet utopique serait de construire un petit immeuble aux formes arrondies en carton et vitre.
            Le but serait de recycler de vieux cartons et papiers pour construire un habitat de long durée.
            Les appartements refleterait la réalité inversée de la société: les plus petits appartements seraient les plus hauts afin que les habitants puissent bénéficier d’une belle vue.
            Inversement, les grands appartements seraient au bas du bâtiment.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Bâtiment',
            'url' => 'batimentCarton1.jpg',
            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton1.jpg',
            "urlThumb" => 'thumb/batimentCarton/batimentCarton1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Bâtiment',
            'url' => 'batimentCarton2.jpg',

            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Bâtiment',
            'url' => 'batimentCarton3.jpg',
            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton3.jpg',

            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Bâtiment',
            'url' => 'batimentCarton4.jpg',
            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton4.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan',
            'url' => 'batimentCarton5.jpg',
            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton5.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan',
            'url' => 'batimentCarton6.jpg',
            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton6.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Plan',
            'url' => 'batimentCarton7.jpg',
            'dir' => 'batimentCarton',
            "urlResized" => 'resized/batimentCarton/batimentCarton7.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '13')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 14,
            'title' => 'Accessoires',
            'class' => 'Service de table - Travail personnel',
            'dateType' => 'Février 2014',

            'materials' => 'carton',
            'description' => 'Une façon différente d’utiliser le carton en revisitant la
            fameuse assiette en carton ainsi que le plateau-repas.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Accessoires',
            'url' => 'accessoiresCarton1.jpg',

            'dir' => 'accessoiresCarton',
            "urlResized" => 'resized/accessoiresCarton/accessoiresCarton1.jpg',
            "urlThumb" => 'thumb/accessoiresCarton/accessoiresCarton1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '14')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Accessoires',
            'url' => 'accessoiresCarton2.jpg',
            'dir' => 'accessoiresCarton',
            "urlResized" => 'resized/accessoiresCarton/accessoiresCarton2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '14')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 15,
            'title' => 'Décorations',
            'class' => 'Bibelot et tableau - Travail personnel',
            'dateType' => 'Février-Mars 2014',
            'dimensions' => '170 x 100 x 40 cm',
            'materials' => 'carton',
            'description' => 'Une façon différente d’utiliser le carton.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Décorations',
            'url' => 'decoCarton1.jpg',
            'dir' => 'decoCarton',
            "urlResized" => 'resized/decoCarton/decoCarton1.jpg',
            "urlThumb" => 'thumb/decoCarton/decoCarton1.jpg',
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '15')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Décorations',
            'url' => 'decoCarton2.jpg',
            'dir' => 'decoCarton',
            "urlResized" => 'resized/decoCarton/accessoiresCarton2.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '15')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 16,
            'title' => 'Living Box L-Element',
            'class' => 'Architecture - Travail collectif',
            'teacher' =>"Rémy Bochet",
            'dimensions' =>"2.30 x 5.90 x 2.40 m",
            'dateType' => 'Février 2014',

            'materials' => 'conteneur et vitre',
            'description' => 'Nous avons pensé un aménagement d’un container de façon à ce que ce soit agréable de vivre à l’intérieur et de profiter de l’extérieur.
            Le but serait de réunir plusieurs modules pour qu’une communauté se créee dans la ville.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Living Box L-Element',
            'url' => 'lElement1.png',
            "urlThumb" => 'thumb/lElement/lElement1.png',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement1.png",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Living Box L-Element',
            'url' => 'lElement2.png',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement2.png",

            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Living Box L-Element',
            'url' => 'lElement3.png',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement3.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Living Box L-Element',
            'url' => 'lElement4.png',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement4.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Living Box L-Element',
            'url' => 'lElement5.png',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement5.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Maquette',
            'url' => 'lElement6.jpg',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement6.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Maquette',
            'url' => 'lElement7.jpg',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement7.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Situation possible',
            'url' => 'lElement9.jpg',
            'dir' => 'lElement',

            "urlResized" => "resized/lElement/lElement9.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);


     /*   Picture::create([
            'name' => 'Contexte possible',
            'url' => 'pictures/lElement/lElement9.jpg',
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '16')->firstOrFail()->id,

        ]);*/


        Project::create([
            'order' => 17,
            'title' => 'Extension Maison Rudin',
            'class' => 'Architecture - Travail collectif',
            'dateType' => 'Mars 2014',
            'teacher' =>"Rémy Bochet",
            'dimensions' =>"9m²",

            'materials' => 'bois',
            'description' => 'Afin de ne pas dénaturer l’oeuvre architecturale de Herzog et De Meuron, nous avons pensé mettre l’annexe sur le toit.
            Elle se distingue par son changement de matériaux mais respecte le bâtiment.
            Herzog et De Meuron voulait donner l’impression que la maison volait et nous avons souhaité garder cet élément fort de cette architecture.',
            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Coupe',
            'url' => 'extensionMaisonRudin1.png',
            "urlThumb" => 'thumb/extensionMaisonRudin/extensionMaisonRudin1.png',
            'dir' => 'extensionMaisonRudin',

            "urlResized" => "resized/extensionMaisonRudin/extensionMaisonRudin1.png",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '17')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Coupe',
            'url' => 'extensionMaisonRudin2.png',
            'dir' => 'extensionMaisonRudin',

            "urlResized" => "resized/extensionMaisonRudin/extensionMaisonRudin2.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '17')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Visualisation',
            'url' => 'extensionMaisonRudin3.png',
            'dir' => 'extensionMaisonRudin',

            "urlResized" => "resized/extensionMaisonRudin/extensionMaisonRudin3.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '17')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Visualisation',
            'url' => 'extensionMaisonRudin4.png',
            'dir' => 'extensionMaisonRudin',

            "urlResized" => "resized/extensionMaisonRudin/extensionMaisonRudin4.png",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '17')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Maquette intérieure',
            'url' => 'extensionMaisonRudin6.jpg',
            'dir' => 'extensionMaisonRudin',

            "urlResized" => "resized/extensionMaisonRudin/extensionMaisonRudin6.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '17')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 18,
            'title' => 'Diane et Alice',
            'class' => 'Dessin - Cours de dessin',
            "teacher" =>"￼Christophe Lombardo",
            'dateType' => 'Janvier 2015',

            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Diane et Alice',
            'url' => 'dianeEtAlice.png',
            "urlThumb" => 'thumb/dianeEtAlice/dianeEtAlice.png',
            'dir' => 'dianeEtAlice',

            "urlResized" => "resized/dianeEtAlice/dianeEtAlice.png",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '18')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 19,
            'title' => 'Entre-temps, brusquement et ensuite...',
            'class' => 'Affiche - Travail individuel',
            "teacher" => 'Christine Keim',
            "materials" =>"Papier, encres et cires",
            "description"=> "Le but de cette affiche est de présenter le film sur un panneau d’affichage extérieur.",
            'dateType' => 'Transversale 1 - Octobre 2013',
            'dimensions' => "Film: Le cours des choses de Fischli & Weiss",



            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Entre-temps, brusquement et ensuite...',
            'url' => 'leCoursDesChoses.jpg',
            "urlThumb" => 'thumb/leCoursDesChoses/leCoursDesChoses.jpg',
            'dir' => 'leCoursDesChoses',

            "urlResized" => "resized/dianeEtAlice/leCoursDesChoses.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '19')->firstOrFail()->id,

        ]);


        Project::create([
            'order' => 20,
            'title' => 'Aspirations',
            'class' => 'Installation - Travail collectif',
            'dateType' => 'Transversale 2 "Les liens" - Février 2014',
            "teacher" => "Paul Viaccoz",
            'dimensions' => '3.60 x 2.40 m environ',
            'materials' => 'gobelets et pailles',
            "description" => "Cette installation a été créée pour représenter les liens entre les enfants
            dans une cour d’école. Le but est de montrer que certains en- fants ne tissent pas de lien.
            Ce qui est intéressant c’est que cette installation fait penser aussi à une cartographie aérienne
            ou de métro et peut en soi, sans explications, représenter totalement autre chose, ce qui laisse
            la liberté au visiteur de penser et développer l’idée de quelque chose qui le touche personnellement."


            // 'url' => '1',
        ]);

//TODO
        Picture::create([
            'name' => 'Aspirations',
            'url' => 'Pailles1.jpg',
            "urlThumb" => 'thumb/aspirations/Pailles1.jpg',
            'dir' => 'aspirations',

            "urlResized" => "resized/aspirations/Pailles1.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '20')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Aspirations',
            'url' => 'Pailles2.jpg',
            'dir' => 'aspirations',

            "urlResized" => "resized/aspirations/Pailles2.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '20')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Aspirations',
            'url' => 'Pailles3.jpg',
            'dir' => 'aspirations',

            "urlResized" => "resized/aspirations/Pailles3.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '20')->firstOrFail()->id,

        ]);

//end TODO
        Project::create([
            'order' => 21,
            'title' => 'Photographies',
            'class' => 'Affiches - Travail individuel',
            'dateType' => 'Cours de photographie - Avril 2014',
            "teacher" => "Christian Marchon",
            "description" => "Ces deux affiches ont été faites pour communiquer un message simple et percutant qui serait destiné à être affiché sur des panneaux dans la ville. Les deux photographies sont simplement des images travaillée, une avec le travail de la surimpression et l’autre avec la luminosité et l’exposition d’un modèle."


            // 'url' => '1',
        ]);


        Picture::create([
            'name' => 'Affiche',
            'url' => 'conservonsLaBanquise.jpg',
            "urlThumb" => 'thumb/photographies/conservonsLaBanquise.jpg',
            'dir' => 'photographies',

            "urlResized" => "resized/photographies/conservonsLaBanquise.jpg",
            'isMainPicture' => '1',
            'project_id' => Project::where('order', '=', '21')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Affiche',
            'url' => 'etSiCetaitToi.jpg',
            'dir' => 'photographies',

            "urlResized" => "resized/photographies/etSiCetaitToi.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '21')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Photographie',
            'url' => 'Surimpression.jpg',
            'dir' => 'photographies',

            "urlResized" => "resized/photographies/Surimpression.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '21')->firstOrFail()->id,

        ]);


        Picture::create([
            'name' => 'Photographie',
            'url' => 'zidanePortraitCynthia.jpg',
            'dir' => 'photographies',

            "urlResized" => "resized/photographies/zidanePortraitCynthia.jpg",
            'isMainPicture' => '0',
            'project_id' => Project::where('order', '=', '21')->firstOrFail()->id,

        ]);

        ///MY PART ///

       /* ProExperience::create([
            'company' => "Promotion Tools",
            'order' => 8,
            'dateStart' => "Oct 2015",
            'dateEnd' => "",
            'jobTitle' => "Promotion de la marque Smartbox et Telion (Oral-B et Braun)",
            'description' => "Promotion et vente dans différents magasins de Suisse Romande en parallèle de mes études",

        ]);

        ProExperience::create([
            'company' => 'GESTOFIN Sàrl, Entreprise de Gestion de Fortune',
            'order' => 7,
            'dateStart' => "2012",
            'dateEnd' => "2015",
            'jobTitle' => "Secrétaire administrative",
            'description' => "A été effectué en parallèle de mes études et de la promotion",

        ]);*/


        /* EXP PROS */

        ProExperience::create([
            'company' => "Promotion Tools",
            'order' => 8,
            'dateStart' => "Oct 2015",
            'dateEnd' => "Présent",
            'jobTitle' => "Promotrice de la marque Smartbox et Telion (Oral-B et Braun)",
            'description' => "Promotion et vente dans différents magasins de Suisse Romande en parallèle de mes études",

        ]);

ProExperience::create([
    'company'              => "GESTOFIN Sàrl, Entreprise de Gestion de Fortune",
    'order'                      => 7,
    'dateStart'              => "2012",
    'dateEnd'              => "Présent",
    'jobTitle'              => "Secrétaire administrative",
    'description'              => "J'effectue ce travail en parallèle de mes études et de la promotion",

]);

ProExperience::create([
    'company'              => "Y K R A Sàrl, atelier d'architecture et de scénographie",
    'order'                      => 6,
    'dateStart'              => "Mars 2015",
    'jobTitle'              => "Stage de deux jours à la construction",
    'description'              => "Réalisation de la scénographie et de l'aménagement de deux salles pour le repas du personnel annuel de Hublot.
                                   Le thème de la soirée était le 'chantier',
                                   donc nous avons tout réalisé en palettes, carrelage industriel basique noir et blanc,
                                    plexiglass translucide, tissus ainsi que lampes de chantier gonflable.",

]);

ProExperience::create([
    'company'              => 'Société Générale, banque',
    'order'                      => 5,
    'dateStart'              => "2011",
    'jobTitle'              => "Stage au service du back - office",
    'description'              => "A été effectué lors de mon apprentissage",

]);

ProExperience::create([
    'company'              => "Electrotel Sàrl, entreprise d'électricité",
    'order'                      => 4,
    'dateStart'              => "2010",
       'dateEnd'              => "2011",
    'jobTitle'              => "Secrétariat général",
    'description'              => "A été effectué lors de mon apprentissage",

]);

ProExperience::create([
    'company'              => 'Ilex FiduTrust, fiduciaire',
    'order'                      => 3,
    'dateStart'              => "2009",
    'dateEnd'              => "2011",
    'jobTitle'              => "Stage au service des déclarations d'impôts",
    'description'              => "A été effectué lors de mon apprentissage",

]);

ProExperience::create([
    'company'              => "Valecourt, entreprise d'investissements et de conseils",
    'order'                      => 2,
    'dateStart'              => "2009",
    'jobTitle'              => "Stage au service du back-office et des courtages",
    'description'              => "A été effectué lors de mon apprentissage",

]);

ProExperience::create([
    'company'              => 'Banque Piguet, banque',
    'order'                      => 1,
    'dateStart'              => "Aoû 2009",
    'jobTitle'              => "Stage au service des courtages et du secrétariat",
    'description'              => "A été effectué lors de mon apprentissage",

]);

        User::create([
            'email'=>'guest@test.com',
            "name" => "guest",
            'password'=>'test'


        ]);

    }

}
