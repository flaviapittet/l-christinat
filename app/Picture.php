<?php namespace App;
/**
 * Picture.php - contains the picture model
 */

use Illuminate\Database\Eloquent\Model;
use Validator;


/**
 * Class picture
 * The model for a picture
 * @package App
 * @author Flemmisatayu
 */
class Picture extends Model {

    /**
     * Timestamps (with softdeleting)
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Gets the shooting event related to a picture
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo a shootingevent
     */

    /**
     * Gets the Modelfc in the picture
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo a modelfc
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    /**
     * Validator for Picture
     * @param array $data
     * @return mixed the validator
     */
     public static function isValid($data = array())
     {
        return Validator::make($data, array(
      
            'id' =>'exists:pictures|sometimes|',
            'name'=>'string|sometimes|required',

            'url'=> 'string|sometimes|required',

            'isMainPicture'=> 'boolean|sometimes|required',

            'project_id' => 'exists:project,id|sometimes|required',
        ))->passes();
    }


    // TESTING WITH MICH


}
