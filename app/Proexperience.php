<?php

namespace App;


use Illuminate\Validation\Validator;
use Illuminate\Database\Eloquent\Model;

class Proexperience extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

   /* public function pictures()
    {
        return $this->hasMany('App\Picture');
    }*/

    public static function isValid($data = array())
    {
        return Validator::make($data, array(

            'id'                      => 'exists:projects|sometimes|required',
            'order'              => 'integer|between:1,50|sometimes|required',
            'name'              => 'string|between:1,50|sometimes|required',
            'company'              => 'string|between:1,250|sometimes|required',
            'location'              => 'string|between:1,250|sometimes|required',
            'dateStart'              => 'string|sometimes|required',
            'dateEnd'              => 'string|sometimes|required',
            'jobTitle'              => 'string|between:1,50|sometimes|required',
            'description'              => 'string|between:1,50|sometimes|required',


        ))->passes();
    }
}
