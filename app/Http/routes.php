<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*Route::get('/projects', function () {
    return view('project.index');
});*/

Route::resource("/mainpictures", "PictureController@getMainPictures");


//Picturecontroller with Michael Z.


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|

*/
Route::group(['middleware' => ['web']], function () {

    Route::resource('/', "ProexperienceController");
});

//Route::group(['middleware' => ['auth']], function () {
Route::get('portfolio', "ProjectController@index");

Route::get('portfolio/{id}', "ProjectController@show");
//});

//testing images
Route::get('/thumbnail/{dir}/{url}', 'ProjectController@createThumbnail');

/**
 * TODO
 * still todo:
 * (- contact form)
 * - middleware auth
 * - small changes lily
 * - deploy
 *
 */

/*Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});*/


/*Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index');
});*/
