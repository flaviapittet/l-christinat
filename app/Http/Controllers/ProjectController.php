<?php


namespace App\Http\Controllers;

use App\Picture;
use App\Project;
use View;
use DB;
use File;
use Response;
use Imagine\Image\Box;
use Imagine;
use App\Services\imageCreator;

class ProjectController extends Controller
{

    // GET /resource
    public function index()
    {
        // Obtenir tous les projects
//      //  $projects = Project::all()->orderBy('order');
//        $projects = DB::table('projects')
//            ->orderBy('order', 'asc')
//            ->get();
        DB::connection()->disableQueryLog();
        $projects = DB::table('projects')
            ->join('pictures', 'projects.id', '=', 'pictures.project_id')
            ->orderBy('order')
            ->where('pictures.isMainPicture', '=', 1)
            /*  ->where('projects.id', '=', 413)*/
            ->get();


//dd($projects);
        foreach ($projects as $project) {

            //ProjectController::createThumbnail($project->dir, $project->url);
            //imageCreator::createThumbnail($project->dir, $project->url);

            $project->urlThumb = "images/portfolio/" . $project->urlThumb;


        }


        // Afficher la vue et donner les projects

        return View::make('project.index')->with('projects', $projects);

    }


//Controller avec .htaccess
//IMG


    public function createThumbnail($dir, $url)
    {
        $inputDir = "../resources/assets/pictures/" . $dir;
        $outputDir = "../resources/assets/pictures/thumb" . '/' . $dir;
        $destinationPath = $outputDir . '/' . $url;


        $width = 750;
        $height = 500;

        //$resized = ->thumbnail($size, $mode);

        if (!File::exists($destinationPath)) {

            if (!File::exists($outputDir)) {

                File::makeDirectory($outputDir);


            }
            if (!File::exists("{$inputDir}/{$url}")) {
                return response('image not found', 404); // todo: make 404 work with theme.

            }

            $imagineImg = Imagine::open("{$inputDir}/{$url}");

            $origWidth = $imagineImg->getSize()->getWidth();

            $origHeight = $imagineImg->getSize()->getHeight();
            $imageRatio = $origWidth / $origHeight;
            if ($imageRatio < 1) {
                $resized = $imagineImg->resize(new Box($height * $imageRatio, $height));
            } else {
                $resized = $imagineImg->resize(new Box($width, $width / $imageRatio));
            }

            $resized->save($destinationPath);


            //$resized = Imagine::open("{$inputDir}/{$url}")->resize(new Box(750, 500));

            /* ->crop(new Box(750, 500));*/
            /*->resize(new Box(750, 500))*/
            $resized->save($destinationPath);
        }

        // Initialize an instance of Symfony's File class.
        // This is a dependency of Laravel so it is readily available.
        $file = new File($destinationPath);

        // Make a new response out of the contents of the file
        // Set the response status code to 200 OK
        /*    $response = Response::make(
                File::get($destinationPath),
                200
            );*/

        // Modify our output's header.
        // Set the content type to the mime of the file.
        // In the case of a .jpeg this would be image/jpeg
        /*  $response->header(
              'Content-type', File::mimeType($destinationPath)
          );
          // We return our image here.*/

        return $destinationPath;
    }

// GET /ma_ressource/create
    public function create()
    {

    }

    // POST /ma_ressource
    public function store()
    {

    }

    // GET /ma_ressource/{id}
    public function show($id)
    {
        $project = DB::table('projects')->where("id", "=", $id)->first();
        $pictures = DB::table('pictures')->where("project_id", "=", $id)->get();
        /*        $pictures = Picture::all();*/
        if (!$project) {
            return "not found with user id : " . $id;
        }

        // dd($pictures);
        foreach ($pictures as $picture) {

            $dir = $picture->dir;
            $url = $picture->url;

            //imageCreator::optimizeImage($dir, $url);
            $picture->url = "../images/portfolio/resized/" . $picture->dir . '/' . $picture->url;
           // dd($picture->url);

        }

        return View::make('project.show')->with('project', $project)->with('pictures', $pictures);

    }



    // GET /ma_ressource/{id}/edit
    public function edit()
    {

    }

    // PUT /ma_ressource/{id}
    public function update()
    {

    }

    // DELETE /ma_ressource/{id}
    public function destroy($id)
    {

    }

}
