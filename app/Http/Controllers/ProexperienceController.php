<?php



namespace App\Http\Controllers;


use View;
use DB;


class ProexperienceController extends Controller {

    // GET /resource
    public function index() {
        // Obtenir toutes les experiences
        $exppros = DB::table('proexperiences')
            ->orderBy('order', 'desc')
            ->get();

        // Afficher la vue et donner les experiences
        return View::make('index')->with('exppros',$exppros);



    }





// GET /ma_ressource/create
    public function create() {

    }

    // POST /ma_ressource
    public function store() {

    }

    // GET /ma_ressource/{id}
    public function show() {

    }

    // GET /ma_ressource/{id}/edit
    public function edit() {

    }

    // PUT /ma_ressource/{id}
    public function update() {

    }

    // DELETE /ma_ressource/{id}
    public function destroy($id) {

    }

}
