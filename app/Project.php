<?php

namespace App;


use Illuminate\Validation\Validator;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

   /* public function pictures()
    {
        return $this->hasMany('App\Picture');
    }*/

    public static function isValid($data = array())
    {
        return Validator::make($data, array(

            'id'                      => 'exists:projects|sometimes|required',

            'order'              => 'integer|between:1,50|sometimes|required',
            'title'              => 'string|between:1,250|sometimes|required',
            'class'              => 'string|between:1,50|sometimes|required',
            'dateType'              => 'string|between:1,50|sometimes|required',
            'teacher'              => 'string|between:1,50|sometimes|required',
            'dimensions'              => 'string|between:1,50|sometimes|required',
            'materials'              => 'string|between:1,150|sometimes|required',
            'description'   => 'string|between:1,500|sometimes|required',
            'url'              => 'string|between:1,255|sometimes|required',

        ))->passes();
    }
}
//