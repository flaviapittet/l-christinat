<?php

namespace App\Services;

use View;
use DB;
use File;
use Response;
use Imagine\Image\Box;
use Imagine;


class ImageCreator{

    public static function createThumbnail($dir, $url)
    {
        $inputDir = "../resources/assets/pictures/" . $dir;
        $outputDir = "images/portfolio/thumb" . '/' . $dir;
        $destinationPath = $outputDir . '/' . $url;


        $width = 750;
        $height = 500;

        //$resized = ->thumbnail($size, $mode);

        if (!File::exists($destinationPath)) {

            if (!File::exists($outputDir)) {

                File::makeDirectory($outputDir);


            }
            if (!File::exists("{$inputDir}/{$url}")) {
                return response('image not found', 404); // todo: make 404 work with theme.

            }

            $imagineImg = Imagine::open("{$inputDir}/{$url}");

            $origWidth = $imagineImg->getSize()->getWidth();

            $origHeight = $imagineImg->getSize()->getHeight();
            $imageRatio = $origWidth / $origHeight;
            if ($imageRatio < 1) {
                $resized = $imagineImg->resize(new Box($height * $imageRatio, $height));
            } else {
                $resized = $imagineImg->resize(new Box($width, $width / $imageRatio));
            }

            $resized->save($destinationPath);


            //$resized = Imagine::open("{$inputDir}/{$url}")->resize(new Box(750, 500));

            /* ->crop(new Box(750, 500));*/
            /*->resize(new Box(750, 500))*/
            $resized->save($destinationPath);
        }

        // Initialize an instance of Symfony's File class.
        // This is a dependency of Laravel so it is readily available.
        $file = new File($destinationPath);

        // Make a new response out of the contents of the file
        // Set the response status code to 200 OK
        /*    $response = Response::make(
                File::get($destinationPath),
                200
            );*/

        // Modify our output's header.
        // Set the content type to the mime of the file.
        // In the case of a .jpeg this would be image/jpeg
        /*  $response->header(
              'Content-type', File::mimeType($destinationPath)
          );
          // We return our image here.*/

        return $destinationPath;
    }


    public static function optimizeImage($dir, $url){
        $inputDir = "../resources/assets/pictures/" . $dir;
        $outputDir = "images/portfolio/resized" . '/' . $dir;
        $destinationPath = $outputDir . '/' . $url;



        $height = 500;



        if (!File::exists($destinationPath)) {

            if (!File::exists($outputDir)) {

                //dd($outputDir);
                File::makeDirectory($outputDir);


            }
            if (!File::exists("{$inputDir}/{$url}")) {
                return response('image not found', 404); // todo: make 404 work with theme.

            }

            $imagineImg = Imagine::open("{$inputDir}/{$url}");

            $origWidth = $imagineImg->getSize()->getWidth();

            $origHeight = $imagineImg->getSize()->getHeight();
            $imageRatio = $origWidth / $origHeight;

                $resized = $imagineImg->resize(new Box($height * $imageRatio, $height));






            $resized->save($destinationPath);
        }

        $file = new File($destinationPath);


        return $destinationPath;

    }
}

?>